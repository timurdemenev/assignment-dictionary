
EXECUTABLE = dictionary
LINKER = ld
COMPILER = nasm
FLAGS = -f elf64

all: main.o lib.o dict.o
	$(LINKER) -o $(EXECUTABLE) $^
main.o: main.asm
	$(COMPILER) $(FLAGS) -o main.o $<
lib.o: lib.asm
	$(COMPILER) $(FLAGS) -o $@ $<
dict.o: dict.asm
	$(COMPILER) $(FLAGS) -o $@ $<
clean:
	rm -rf *.o $(EXECUTABLE)
