%define val 0

%macro colon 2
	%ifid %2
		%2:
    		dq val
    		%define val %2 
    %else
    	%fatal "Argument 2 must be a label"
    %endif
    %ifstr %1
     	db %1, 0
    %else
    	%fatal "Argument 1 must be a string"
    %endif
%endmacro
