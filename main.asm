section .text

%include "colon.inc"

%define PTR_SIZE 8

extern read_word
extern print_string
extern print_newline
extern exit
extern string_length
extern read_char
extern find_word
	
section .data
%include "words.inc"
	greet_mes: db 'Enter key: ', 0
	no_found_err: db 'No data', 0ah, 0
	long_err: db 'Key is too long', 0ah, 0
	buffer: times 255 db 0

section .text

global _start

_start:
    mov rdi, greet_mes
    call print_string
    
	mov rdi, buffer
	mov rsi, 255
    call read_word
    
    test rax, rax
    jz .long_key_error
    
    mov rdi, rax
    mov rsi, val
    call find_word
    
    test rax, rax
    jz .no_found_error
    
    mov rdi, rax
    add rdi, PTR_SIZE
    add rdi, rdx
   	inc rdi
   	
   	call print_string
   	call print_newline
   	call exit
.no_found_error:
	mov rdi, no_found_err
	call abort
.long_key_error:
	mov rdi, long_err
	call abort

abort:
	push rdi
	call string_length
	pop rdi
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 2
    mov rax, 1
    syscall
    call exit
