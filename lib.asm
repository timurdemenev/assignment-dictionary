section .text

global exit
global print_string
global string_length
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
    syscall
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:  
	cmp byte [rdi + rax], 0
    jz  .end
    inc rax
    jmp .loop
.end:
	ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rdx, rax
    mov rsi, rdi
    mov rdi, 1
    mov rax, 1
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rax, 1
    mov rdi, 1
    lea rsi, [rsp]
    mov rdx, 1
    syscall
    pop rdi
    xor rdi, rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0ah
	call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	mov r9, 10
	mov rax, rdi
	mov rdi, rsp
	dec rdi
	push 0
	sub rsp, 16

.loop:
	xor rdx, rdx
	div r9
	add rdx, '0'
	dec rdi
	mov [rdi], dl
	cmp rax, 0
	je .end
	jmp .loop

.end:
	call print_string
	add rsp, 24
	ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    cmp rdi, 0
    jge .printing_int
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
.printing_int:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
    mov rdx, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    cmp rax, rdx
    jne .not_eq
.checking:
    cmp rax, 0
    jl .eq
    mov cl, byte[rsi + rax]
    cmp byte[rdi + rax], cl
    jne .not_eq
    dec rax
    jmp .checking
.eq:
    mov rax, 1
    ret
.not_eq:
    xor rax, rax
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
	xor rax, rax
	xor rdi, rdi
    push 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
	xor rdx, rdx
.read_spaces_loop:
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0x20
	je .read_spaces_loop
	cmp rax, 0x9
	je .read_spaces_loop
	cmp rax, 0xA
	je .read_spaces_loop
.read_word_loop:
	cmp rax, 0
	je .complete_word
	mov byte[rdi + rdx], al
	inc rdx
	cmp rdx, rsi
	jge .fail_read
	push rdi
	push rsi
	push rdx
	call read_char
	pop rdx
	pop rsi
	pop rdi
	cmp rax, 0x20
	je .complete_word
	cmp rax, 0x9
	je .complete_word
	cmp rax, 0xA
	je .complete_word
	jmp .read_word_loop
.complete_word:
	mov byte[rdi + rdx], 0
	mov rax, rdi
	jmp .end_read_word
.fail_read:
	xor rax, rax
	xor rdx, rdx
.end_read_word:
    ret


; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    push rbx
.non_digits_loop:
    cmp byte[rdi + rdx], 0
	je .end_parse_uint
    cmp byte[rdi + rdx], '0'
    jl .next_step
    cmp byte[rdi + rdx], '9'
    jle .parse_uint_loop
.next_step:
	inc rdi
    jmp .non_digits_loop
.parse_uint_loop:
	cmp byte[rdi + rdx], 0
	je .end_parse_uint
    cmp byte[rdi + rdx], '0'
    jl .end_parse_uint
    cmp byte[rdi + rdx], '9'
    jg .end_parse_uint
    sal rax, 1
    mov rbx, rax
    sal rax, 2
    add rax, rbx
    xor rbx, rbx
    mov bl, [rdi + rdx]
    sub rbx, '0'
    add rax, rbx
    inc rdx
    jmp .parse_uint_loop
.fail_parse_uint:
	xor rdx, rdx
.end_parse_uint:
	pop rbx
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx
    push rbx
    mov rbx, 1
.non_digits_loop:
    cmp byte[rdi + rdx], 0
	je .end_parse_int
	cmp byte[rdi + rdx], '-'
	jne .next_cond
	mov rbx, -1
	jmp .parsing_uint
.next_cond:
    cmp byte[rdi + rdx], '0'
    jl .int_next_step
    cmp byte[rdi + rdx], '9'
    jle .parsing_uint
.int_next_step:
	inc rdi
	jmp .non_digits_loop
.parsing_uint:
	call parse_uint
	cmp rdx, 0
	je .end_parse_int
	cmp rbx, 0
	jge .end_parse_int
	neg rax
	inc rdx
.end_parse_int:
	pop rbx
    ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jge .len1_gt_len2
    mov rdx, rax
    jmp .before_copy
.len1_gt_len2:
	xor rax, rax
.before_copy:
    push rbx
.copy_loop:
	xor rbx, rbx
    mov bl, byte[rdi]
    mov [rsi], rbx
    inc rdi
    inc rsi
    dec rdx
    cmp rdx, 0
    jg .copy_loop
.end_cop:
	pop rbx
    ret
