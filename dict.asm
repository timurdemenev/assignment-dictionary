section .text

global find_word

extern string_equals

%define PTR_SIZE 8

;rdi - key
;rsi - ptr to dictionary

;[label, word]

find_word:
.loop:
    push rsi
    push rdi
    add rsi, PTR_SIZE
    call string_equals
    pop rdi
    pop rsi
    test rax, rax
    jnz .success
    mov rsi, [rsi]
    test rsi, rsi
    jz .fail
    jmp .loop
.success:
	mov rax, rsi
	ret
.fail:
	xor rax, rax
    ret
